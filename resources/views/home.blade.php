@extends('adminlte::page')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @livewire('arbol.index')
    </div>
</div>
@endsection
