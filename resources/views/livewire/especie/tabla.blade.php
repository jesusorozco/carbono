
<h2>Listado de Especies</h2>
<table class="table-stripped table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Especie</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($especies as $especie)
            <tr>
                <td>{{ $especie->id }}</td>
                <td>{{ $especie->especie }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

{{ $especies->links() }}
