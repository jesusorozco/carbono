<div>
    <div class="row">
        <div class="col-sm-9">
            <input type="text" name="" id="" placeholder="buscar" class="form-control" wire:model="busqueda">
        </div>
        <div class="col-sm-3">
            <select name="" id="" class="form-select" wire:model="paginacion">
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="50">50</option>
                <option value="100">100</option>
            </select>
        </div>
    </div>
    <table class="table table-stripped table-hover">
        <thead>
            <tr>
                <th>Especie</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($especies as $especie)
                <tr>
                    <td>{{ $especie->especie }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $especies->links() }}
</div>
{{-- <div>
    @include('livewire.especie.tabla')
</div> --}}
