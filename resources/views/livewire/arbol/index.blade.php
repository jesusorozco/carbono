<div>
    <div class="row">
        <div class="col-sm-9">
            <input type="text" name="" id="" placeholder="buscar" class="form-control" wire:model="busqueda">
        </div>
        <div class="col-sm-3">
            <select name="" id="" class="form-select" wire:model="paginacion">
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="50">50</option>
                <option value="100">100</option>
            </select>
        </div>
    </div>
    <table class="table table-stripped table-hover">
        <thead>
            <tr>
                <th>Conglomerado</th>
                <th>Sitio</th>
                <th>Numero_arbol</th>
                <th>Familia</th>
                <th>Genero</th>
                <th>Epiteto</th>
                <th>Diametro</th>
                <th>Altura</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($arboles as $arbol)
                <tr>
                    <td>{{ $arbol->conglomerado }}</td>
                    <td>{{ $arbol->sitio }}</td>
                    <td>{{ $arbol->numero_arbol }}</td>
                    <td>{{ $arbol->familia }}</td>
                    <td>{{ $arbol->genero }}</td>
                    <td>{{ $arbol->epiteto }}</td>
                    <td>{{ $arbol->diametro }}</td>
                    <td>{{ $arbol->altura }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $arboles->links() }}
</div>
{{-- <div>
    @include('livewire.arbol.tabla')
</div> --}}
