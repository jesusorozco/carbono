<?php

namespace App\Lib;
use Illuminate\Support\Str;
use App\Models\Funcion;
use App\Models\Variable;

class Formula
{
    public $ecuacion_original;
    public $ecuacion;
    public $ecuacion_php;
    public $variables = [];
    public $constantes = [];
    public $funciones = [];
    public $errores = [];
    private $palabra = '';
    private $estatus_anterior = '';
    private $pila;

    function __construct($ecuacion)
    {
        $this->ecuacion_original = $ecuacion;
        $this->ecuacion = $ecuacion;
        $this->limpiaCadena();
        $this->separaTokens();
        $this->preparaArrays();
    }

    private function clasificaPalabra($tipo, $palabra)
    {
        if($tipo == 'constantes') {
            array_push($this->$tipo, $this->palabra);
        }
        else if($abreviacion = Variable::abreviacionCorrecta($palabra)) {
            array_push($this->variables, $abreviacion);
            $palabra = '[' . $abreviacion . ']';
        }
        else if(Funcion::firstWhere('abreviacion', 'like', $palabra)) {
            array_push($this->funciones, $palabra);
        }
        else {
            array_push($this->errores, $palabra);
        }

        $this->pila .= $palabra;
    }

    private function estableceEstatus($accion, $tipo = null, $caracter = null)
    {
        switch ($accion) {
            case 'inicia':
                $this->palabra = $caracter;
                $this->estatus_anterior = $tipo;
                break;
            case 'acumula':
                $this->palabra .= $caracter;
                $this->estatus_anterior = $tipo;
                break;
            case 'guarda':
                $this->clasificaPalabra($tipo, $this->palabra);
                $this->palabra = '';
                $this->estatus_anterior = '';
                break;
            case 'guarda caracter':
                $this->pila .= $caracter;
                break;
            case 'quita negativo':
                $this->palabra = substr($this->palabra, 1);
                $this->palabra .= $caracter;
                $this->estatus_anterior = $tipo;
                $this->pila .= '-';
                break;
            case 'borra':
                $this->palabra = '';
                $this->estatus_anterior = '';
                $this->pila .= $caracter;
                break;
            
            default:
                # code...
                break;
        }
    }

    private function separaTokens()
    {
        $this->ecuacion .= '|'; // Separador de fin de la ecuación para identificar que terminó el ciclo y acumular el caracter pendiente

        for ($i=0; $i < Str::of($this->ecuacion)->length(); $i++) { //Recorremos la ecuación caracter por caracter para definir a que tipo de valor corresponde

            $caracter = substr($this->ecuacion, $i, 1);

            switch ($this->tipoValor($caracter)) {
                
                case 'letra': // --------------------------------------------------------------------------
                    if($this->estatus_anterior == ''/* || $this->estatus_anterior == 'punto'*/) {
                        $this->estableceEstatus('inicia', 'letra', $caracter);
                    }
                    else if($this->estatus_anterior == 'letra' || $this->estatus_anterior == 'numero') {
                        $this->estableceEstatus('acumula', 'letra', $caracter);
                    }
                    else if($this->estatus_anterior == 'numero_negativo') {
                        $this->estableceEstatus('quita negativo', 'letra', $caracter);
                        $this->estableceEstatus('guarda caracter', null, '-');
                    }
                    else if($this->estatus_anterior == 'negativo') {
                        $this->estableceEstatus('inicia', 'letra', $caracter);
                        $this->estableceEstatus('guarda caracter', null, '-');
                    }
                    else if($this->estatus_anterior == 'punto') {
                        $this->estableceEstatus('inicia', 'letra', $caracter);
                        $this->estableceEstatus('guarda caracter', null, '.');
                    }
                    break;

                case 'numero': // --------------------------------------------------------------------------
                    if($this->estatus_anterior == '') {
                        $this->estableceEstatus('inicia', 'numero', $caracter);
                    }
                    else if($this->estatus_anterior == 'numero' || $this->estatus_anterior == 'punto') {
                        $this->estableceEstatus('acumula', 'numero', $caracter);
                    }
                    else if($this->estatus_anterior == 'numero_negativo' || $this->estatus_anterior == 'negativo') {
                        $this->estableceEstatus('acumula', 'numero_negativo', $caracter);
                    }
                    else if($this->estatus_anterior == 'letra') {
                        $this->estableceEstatus('acumula', 'letra', $caracter);
                    }
                    break;

                case 'punto': // --------------------------------------------------------------------------
                    if($this->estatus_anterior == '') {
                        $this->estableceEstatus('inicia', 'punto', $caracter);
                    }
                    else if($this->estatus_anterior == 'numero' || $this->estatus_anterior == 'numero_negativo') {
                        $this->estableceEstatus('acumula', 'punto', $caracter);
                    }
                    else if($this->estatus_anterior == 'negativo') { // Aquí habrá que revisar después
                        $this->estableceEstatus('acumula', 'negativo', $caracter);
                        // $this->estableceEstatus('borra', null, '-');
                    }
                    else if($this->estatus_anterior == 'letra') {
                        $this->estableceEstatus('guarda', 'palabras');
                    }
                    break;

                case 'simbolo': // --------------------------------------------------------------------------
                    if($this->estatus_anterior == '') {
                        $this->estableceEstatus('guarda caracter', null, $caracter);
                        $this->estableceEstatus('borra');
                    }
                    else if($this->estatus_anterior == 'numero' || $this->estatus_anterior == 'numero_negativo') {
                        $this->estableceEstatus('guarda', 'constantes');
                        $this->estableceEstatus('guarda caracter', null, $caracter);
                        $this->estableceEstatus('borra');
                    }
                    else if($this->estatus_anterior == 'letra') {
                        $this->estableceEstatus('guarda', 'palabras');
                        $this->estableceEstatus('guarda caracter', null, $caracter);
                        $this->estableceEstatus('borra');
                    }
                    else if($this->estatus_anterior == 'negativo') {
                        $this->estableceEstatus('guarda caracter', null, '-');
                        $this->estableceEstatus('guarda caracter', null, $caracter);
                        $this->estableceEstatus('borra');
                    }
                    break;

                case 'negativo': // --------------------------------------------------------------------------
                    if($this->estatus_anterior == '') {
                        $this->estableceEstatus('inicia', 'negativo', $caracter);
                    }
                    else if($this->estatus_anterior == 'numero' || $this->estatus_anterior == 'numero_negativo' ) { /*&& $this->esNumero($this->palabra)*/
                        $this->estableceEstatus('guarda', 'constantes');
                        $this->estableceEstatus('inicia', 'negativo', $caracter);;
                    }
                    else if($this->estatus_anterior == 'letra') {
                        $this->estableceEstatus('guarda', 'palabras');
                        $this->estableceEstatus('inicia', 'negativo', $caracter);
                    }
                    else if($this->estatus_anterior == 'negativo') {
                        $this->estableceEstatus('guarda caracter', '-');
                        $this->estableceEstatus('inicia', 'negativo', $caracter); // Está de más por que el estatus anterior ya es negativo
                    }
                    else {
                        $this->estableceEstatus('guarda caracter', null, $caracter);
                    }
                break;

                case 'fin': // --------------------------------------------------------------------------

                    $this->ecuacion = substr($this->ecuacion, 0, -1); // Quitamos el separador de fin de la ecuación

                    if($this->estatus_anterior == 'numero' || $this->estatus_anterior == 'numero_negativo') {
                        $this->estableceEstatus('guarda', 'constantes');
                    }
                    else if($this->estatus_anterior == 'letra') {
                        $this->estableceEstatus('guarda', 'palabras');
                    }
                    else if($this->estatus_anterior == 'negativo') {
                        $this->estableceEstatus('guarda caracter', null, '-');
                    }
                    break;
                
                default: // ---------------------------------------------------------------------------------
                    $this->estableceEstatus('borra');
                    break;
            }
        }
    }

    /**
     * Quitando espacios en blanco, caracteres nulos, tabulaciones y saltos de línea
     * Además convirtiendo la cadena a minúsculas
     */
    private function limpiaCadena()
    {
        // $this->ecuacion = trim(strtolower($this->ecuacion));
        $this->ecuacion = strtolower(preg_replace('/[^0-9a-zA-Z\.\+\-\*\/\^\(\)\ñ\Ñ\_]/', '', $this->ecuacion));
    }

    private function tipoValor($texto)
    {
        if($this->esLetra($texto))
            return 'letra';
        else if($this->esNumero($texto))
            return 'numero';
        else if($this->esPunto($texto))
            return'punto';
        else if($this->esSimbolo($texto))
            return'simbolo';
        else if($this->esNegativo($texto))
            return'negativo';
        else if($this->esFin($texto))
            return'fin';
        else
            return null;
    }

    private function preparaArrays() {

        $this->ecuacion = $this->pila;

        $variables_temp = array_unique($this->variables);
        $this->variables = [];
        foreach ($variables_temp as $valor) {
            array_push($this->variables, $valor);
        }

        $funciones_temp = array_unique($this->funciones);
        $this->funciones = [];
        foreach ($funciones_temp as $valor) {
            array_push($this->funciones, $valor);
        }
        
        $this->ecuacionPhp();
    }

    private function ecuacionPhp() {

        // Reemplazando acento circunflejo (símbolo de potencia "^") ya que en php los números se elevan con 2 asteriscos "**"
        $this->ecuacion_php = str_replace('^', '**', $this->ecuacion);

        // Reemplazando la abreviación del logaritmo natural
        $this->ecuacion_php = str_replace('ln', 'log', $this->ecuacion_php);
    }

    private function esLetra($caracter)
    {
        return ctype_alpha($caracter) || $caracter === 'ñ' || $caracter === 'Ñ' || $caracter === '_';
        // return ctype_alpha($caracter);
    }

    private function esNumero($cadena)
    {
        return is_numeric($cadena);
        // return (float)$cadena;
    }

    private function esPunto($caracter)
    {
        return $caracter === '.';
        // return $caracter === ',' || $caracter === '.';
    }

    private function esSimbolo($caracter)
    {
        return strpos(' / ( ) ^ + *', $caracter);
    }

    private function esNegativo($caracter)
    {
        return $caracter === '-';
    }

    private function esFin($caracter)
    {
        return $caracter === '|';
    }
}
