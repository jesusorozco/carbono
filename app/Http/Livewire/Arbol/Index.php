<?php

namespace App\Http\Livewire\Arbol;

use Livewire\Component;
use App\Models\Arbol;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;
    public $busqueda = '';
    public $paginacion = 10;
    protected $paginationTheme = 'bootstrap';

    protected $queryString = [
        'busqueda' => ['except' => ''],
        'paginacion' => ['except' => 10],
    ];

    public function render()
    {
        $arboles = $this->consulta();
        $arboles = $arboles->paginate($this->paginacion);

        if($arboles->currentPage() > $arboles->lastPage()) {
            $this->resetPage();
            $arboles = $this->consulta();
            $arboles = $arboles->paginate($this->paginacion);
        }

        $params = [
            'arboles' => $arboles
        ];

        return view('livewire.arbol.index', $params);
    }

    private function consulta()
    {
        $query = Arbol::orderByDesc('id');

        if($this->busqueda != '') {
            $query->where('conglomerado', '=', $this->busqueda);
        }

        return $query;
    }
}
