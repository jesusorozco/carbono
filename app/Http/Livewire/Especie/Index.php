<?php

namespace App\Http\Livewire\Especie;

use Livewire\Component;
use App\Models\Especie;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;
    public $busqueda = '';
    public $paginacion = 10;
    protected $paginationTheme = 'bootstrap';

    protected $queryString = [
        'busqueda' => ['except' => ''],
        'paginacion' => ['except' => 10],
    ];

    public function render()
    {
        $especies = $this->consulta();
        $especies = $especies->paginate($this->paginacion);

        if($especies->currentPage() > $especies->lastPage()) {
            $this->resetPage();
            $especies = $this->consulta();
            $especies = $especies->paginate($this->paginacion);
        }

        $params = [
            'especies' => $especies
        ];

        return view('livewire.especie.index', $params);
    }

    private function consulta()
    {
        $query = Especie::orderByDesc('id');

        if($this->busqueda != '') {
            $query->where('especie', 'LIKE', '%' . $this->busqueda . '%');
        }

        return $query;
    }
}
