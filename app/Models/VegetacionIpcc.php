<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VegetacionIpcc extends Model
{
    use HasFactory;
    protected $table = 'vegetacion_ipcc';
    public $timestamps = false;

    public static function get($clave)
    {
        $vegetacion_ipcc = VegetacionIpcc::where('clave' , $clave)
            ->firstOr(
                function() {
                    return $obj = (object) array('id' => null);
                }
            );

        return $vegetacion_ipcc;
    }
}
