<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conglomerado extends Model
{
    use HasFactory;

    public static function get($numero)
    {
        $conglomerado = Conglomerado::where('numero' , $numero)
            ->firstOr(
                function() {
                    return $obj = (object) array('id' => null);
                }
            );

        return $conglomerado;
    }
}
