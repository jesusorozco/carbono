<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sitio extends Model
{
    use HasFactory;

    public static function get($conglomerado, $sitio)
    {
        $sitio = Sitio::where('conglomerado' , $conglomerado)
            ->where('numero', $sitio)
            ->firstOr(
                function() {
                    return $obj = (object) array('id' => null);
                }
            );

        return $sitio;
    }
}
