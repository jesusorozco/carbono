<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Arbol extends Model
{
    use HasFactory;
	protected $table = 'arboles';
    public $fillable = ['anio_levantamiento', 'conglomerado', 'sitio', 'condicion', 'especie_id', 'familia', 'genero', 'epiteto', 'categoria_infra', 'infraespecie', 'numero_arbol', 'numero_tallo', 'tallos', 'diametro', 'altura',  'referencia_1', 'referencia_2'];
}
