<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VegetacionBur extends Model
{
    use HasFactory;
    protected $table = 'vegetacion_bur';
    public $timestamps = false;

    public static function get($clave)
    {
        $vegetacion_bur = VegetacionBur::where('clave' , $clave)
            ->firstOr(
                function() {
                    return $obj = (object) array('id' => null);
                }
            );

        return $vegetacion_bur;
    }
}
