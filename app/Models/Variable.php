<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    use HasFactory;
    public $fillable = ['nombre', 'abreviacion', 'unidad', 'variable_id'];

    public static function abreviacionCorrecta($palabra) {
          
        if($registro = Variable::firstWhere('abreviacion', 'like', $palabra)) {

             if($registro->variable_id) {
                 return Variable::firstWhere('id', $registro->variable_id)->abreviacion;
             }
 
             return $registro->abreviacion;
         }
         
         return false;
   }
}
