<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VegetacionInegi extends Model
{
    use HasFactory;
    protected $table = 'vegetacion_inegi';
    public $timestamps = false;
}
