<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ecorregion extends Model
{
    use HasFactory;
    protected $table = 'ecorregiones';
    public $timestamps = false;

    public function ecuaciones()
    {
        return $this->belongsToMany(Ecuacion::class);
    }

    public static function get($clave, $nivel = 4)
    {
        $clave = $clave === null ? '' : $clave; //Limpiando clave para que si se envía un nulo, siga funcionando el LIKE

        while($nivel > 0) {
            
            $ecorregion = Ecorregion::where('clave', 'LIKE', $clave)
                ->where('nivel', $nivel)
                ->firstOr(
                    function() {
                        return $obj = (object) array('id' => null);
                    }
                );
            
            if ( $ecorregion->id )
                $nivel = 0;
            else
                $nivel --;
        }

        return $ecorregion;
    }
}
