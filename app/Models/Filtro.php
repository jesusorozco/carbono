<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Filtro extends Model
{
    use HasFactory;
    public $fillable = ['asignacion_criterios_id', 'variable_resultado', 'campo_1', 'campo_2', 'criterio'];
}
