<?php

namespace App\Imports;

use App\Models\Funcion;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class FuncionImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Funcion([
            'nombre'      => $row['nombre'],
            'abreviacion'    => $row['abreviacion'],
            'funcion_id' => $row['funcion_id'],
        ]);
    }
}
