<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\Conglomerado;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Support\Facades\DB;

class ConglomeradoImport implements ToCollection, WithHeadingRow, WithChunkReading
{
    /**
    * @param Collection $collection
    */
    private $conglomerados = "";
    private $contador = 0;

    public function __construct()
    {
        dump('Inicia');
        $this->conglomerados = collect([]);
        $this->listar_conglomerados_existentes();
    }

    function __destruct() {
        // print "Destruyendo " . __CLASS__ . "\n";
    }

    public function collection(Collection $conglomerados)
    {
        foreach ($conglomerados as $conglomerado) {

            print 'Buscando conglomerado ' . $conglomerado['conglomerado'] . '\n';

            if( !$this->existe_conglomerado($conglomerado['conglomerado']) ) {

                $this->conglomerados->push($conglomerado['conglomerado']);

                try {
                    $this->contador++;

                    print "Insertando conglomerado\n";

                    return new Conglomerado([
                        'numero'            => $conglomerado['conglomerado'],
                        'upmid'             => $conglomerado['upmid'],
                    ]);
                } catch (\Throwable $th) {

                    // Si no se pudo insertar el conglomerado se borra de la lista
                    // array_pop($this->conglomerados);
                    // $this->contador--;
                }
            }
        }
    }
    
    public function chunkSize(): int
    {
        return 5000;
    }

    private function existe_conglomerado($conglomerado)
    {
        // return $this->conglomerados->firstWhere('conglomerado', $conglomerado);
        return $this->conglomerados->contains($conglomerado);
    }

    private function listar_conglomerados_existentes()
    {
        $conglomerados = Conglomerado::select([DB::raw('numero AS conglomerado')])->get();
        $this->conglomerados = $conglomerados->pluck('conglomerado');
    }
}
