<?php

namespace App\Imports;

use App\Models\Ecorregion;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EcorregionImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Ecorregion([
            'clave'                 => $row['clave'],
            'descripcion'           => $row['descripcion'],
            'nivel'                 => $row['nivel'],
            'clave_eco1'            => $row['clave_eco1'],
            'clave_eco2'            => $row['clave_eco2'],
            'clave_eco3'            => $row['clave_eco3'],
            'clave_eco4'            => $row['clave_eco4'],
            'ecorregion_id'         => $row['ecorregion_id'],
        ]);
    }
}
