<?php

namespace App\Imports;

use App\Models\VegetacionInegi;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class VegetacionInegiImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new VegetacionInegi([
            'clave'             => $row['clave'],
            'nombre'            => $row['nombre'],
            'fase'              => $row['fase'],
            'clave_base'        => $row['clave_base'],
            'clave_fase'        => $row['clave_fase'],
            'tipo'              => $row['tipo'],
            'es_primaria'       => $row['es_primaria'],
            'es_secundaria'     => $row['es_secundaria'],
            'es_bosque'         => $row['es_bosque'],
            'serie'             => $row['serie'],
            'id_mn'             => $row['id_mn'],
            'activo'            => $row['activo'],
        ]);
    }
}
