<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Models\Conglomerado;
use App\Models\Sitio;
use App\Imports\EstatusSitioImport;

class SitioImport
{
    private $sitios = "";
    private $contador = 0;
    private $estatus = "";

    public function __construct()
    {
        $this->estatus = new EstatusSitioImport;
        $this->sitios = collect([]);
        $this->listar_sitios_existentes();
    }

    function __destruct() {
        // print "Destruyendo " . __CLASS__ . "\n";
        // print $this->contador . "conglomerados insertados\n";
    }

    public function import($sitios)
    {
        foreach ($sitios as $sitio) {

            if( !$this->existe_sitio($sitio['conglomerado'], $sitio['sitio']) ) {   

                $this->sitios->push(['conglomerado' => $sitio['conglomerado'], 'sitio' => $sitio['sitio']]);

                try {

                    $this->contador++;

                    Sitio::create([
                        'numero'                => $sitio['sitio'],
                        'superficie_metros'     => 400,
                        'conglomerado'          => $sitio['conglomerado'],
                        'conglomerado_id'       => Conglomerado::firstWhere('numero', $sitio['conglomerado'])['id'],
                    ]);

                } catch (\Throwable $th) { // Si no se pudo insertar el sitio se borra de la lista
                    $this->sitios->pop();
                    $this->contador--;
                }
            }
        }
        
        $this->estatus->import($sitios);
    }

    private function existe_sitio($conglomerado, $sitio)
    {
        return $this->sitios->where('conglomerado', $conglomerado)->where('sitio', $sitio)->count() ? true : false;
    }

    private function listar_sitios_existentes()
    {
        $this->sitios = Sitio::select(['conglomerado', DB::raw('numero AS sitio')])->get();
        
        // $sitios = Sitio::select(['conglomerado', DB::raw('numero AS sitio')])->get();
        // foreach ($sitios as $sitio) {
        //     $this->sitios->push(['conglomerado' => $sitio['conglomerado'], 'sitio' => $sitio['sitio']]);
        // }

    }
}
