<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use App\Lib\Formula;
use App\Models\Ecuacion;
use App\Models\Ecorregion;
// use DB;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class EcuacionImport implements ToCollection, WithHeadingRow, WithChunkReading
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach ($collection as $row) 
        {
            $formula = new Formula($row['ecuacion']);
            $ecuacion_php = $formula->ecuacion_php;
            $ecuacion_original = str_replace('[', '', $formula->ecuacion);
            $ecuacion_original = str_replace(']', '', $ecuacion_original);
            $numero_variables = count($formula->variables);

            // $ecuacion = Ecuacion::firstOrCreate([
            //                 'variable'              => $row['variable'],
            //                 'uso'                   => $row['uso'],
            //                 'forma'                 => $row['forma'],
            //                 'tipo'                  => $row['tipo'],
            //                 'ecuacion'              => $ecuacion_php,
            //                 'familia'               => $row['familia'],
            //                 'genero'                => $row['genero'],
            //                 'epiteto'               => $row['epiteto'],
            //                 'condicion'             => $row['condicion'],
            //                 'componente'            => $row['componente'],
            //                 'diametro_min'          => $row['diametro_min'],
            //                 'diametro_max'          => $row['diametro_max'],
            //                 'numero_arboles'        => $row['numero_arboles'],
            //                 'r2'                    => $row['r2'],
            //                 'numero_variables'      => $numero_variables,
            //                 'grado_putrefaccion'    => $row['grado_putrefaccion'],
            //                 'anio_publicacion'      => $row['anio_publicacion'],
            //                 'activo'                => $row['activo'],
            //                 'ecuacion_original'     => $ecuacion_original,
            //             ]);

            $ecuacion = Ecuacion::updateOrCreate([
                                'variable'              => $row['variable'],
                                'ecuacion'              => $ecuacion_php,
                                'familia'               => $row['familia'],
                                'genero'                => $row['genero'],
                                'epiteto'               => $row['epiteto'],
                                'condicion'             => $row['condicion'],
                                'componente'            => $row['componente'],
                                'anio_publicacion'      => $row['anio_publicacion'],
                            ],
                            [
                                'uso'                   => $row['uso'],
                                'forma'                 => $row['forma'],
                                'tipo'                  => $row['tipo'],
                                'diametro_min'          => round($row['diametro_min'], 2),
                                'diametro_max'          => round($row['diametro_max'], 2),
                                'numero_arboles'        => $row['numero_arboles'],
                                'r2'                    => round($row['r2'], 4),
                                'numero_variables'      => $numero_variables,
                                'grado_putrefaccion'    => $row['grado_putrefaccion'],
                                'activo'                => $row['activo'],
                                'ecuacion_original'     => $ecuacion_original,
                            ]
                        );

            // $parametros = [
            //                 'variable'              => $row['variable'],
            //                 'uso'                   => $row['uso'],
            //                 'forma'                 => $row['forma'],
            //                 'tipo'                  => $row['tipo'],
            //                 'ecuacion'              => $ecuacion_php,
            //                 'familia'               => $row['familia'],
            //                 'genero'                => $row['genero'],
            //                 'epiteto'               => $row['epiteto'],
            //                 'condicion'             => $row['condicion'],
            //                 'componente'            => $row['componente'],
            //                 'diametro_min'          => $row['diametro_min'],
            //                 'diametro_max'          => $row['diametro_max'],
            //                 'numero_arboles'        => $row['numero_arboles'],
            //                 'r2'                    => $row['r2'],
            //                 // 'numero_variables'      => $numero_variables,
            //                 'grado_putrefaccion'    => $row['grado_putrefaccion'],
            //                 'anio_publicacion'      => $row['anio_publicacion'],
            //                 'activo'                => $row['activo'],
            //                 'ecuacion_original'     => $ecuacion_original,
            //             ];
            

            // if(!$this->existe_ecuacion($parametros)) {
            //     Ecuacion::Create($parametros);
            // }

            
            if($ecorregion = Ecorregion::where('clave', 'like', "" . $row["clave_ecorregion_n4"] . "")->first())
            {
                $this->insertaEcuacionEcorregion($ecuacion->id, $ecorregion->id);
            }
            elseif($ecorregion = Ecorregion::where('clave', 'like', "" . $row["clave_ecorregion_n3"] . "")->first())
            {
                $this->insertaEcuacionEcorregion($ecuacion->id, $ecorregion->id);
            }
            elseif($ecorregion = Ecorregion::where('clave', 'like', "" . $row["clave_ecorregion_n2"] . "")->first())
            {
                $this->insertaEcuacionEcorregion($ecuacion->id, $ecorregion->id);
            }
            elseif($ecorregion = Ecorregion::where('clave', 'like', "" . $row["clave_ecorregion_n1"] . "")->first())
            {
                $this->insertaEcuacionEcorregion($ecuacion->id, $ecorregion->id);
            }
        }
    }

    public function chunkSize(): int {

        return 100;
    }

    private function insertaEcuacionEcorregion($ecuacion_id = null, $ecorregion_id = null) {

        if($ecuacion_id && $ecorregion_id) {
            DB::table('ecuacion_ecorregion')->insertOrIgnore([
                'ecuacion_id'       => $ecuacion_id,
                'ecorregion_id'     => $ecorregion_id,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ]);
        }
    }

    private function existe_ecuacion($parametros) {

        $existe = Ecuacion::whereRaw("1 = 1")
            ->where(function($query) use ($parametros) {
            foreach ($parametros as $parametro => $valor) {
                if($this->es_decimal($valor)) {
                    $query->where(DB::raw('ROUND(' . $parametro . ', 4)'), "=", round($valor, 4));
                }   else {
                    $query->where($parametro, "=", $valor);
                }
                    // ->orWhere(function($query_null) use($parametro, $parametros) {
                    //     $query_null->whereNull($parametro)
                    //             ->whereRaw("'" . $parametros[$parametro] . "'" . " = ''");
                    // });
            }
        })
        ->first();

         return $existe;
    }

    // https://stackoverflow.com/questions/6772603/check-if-number-is-decimal
    private function es_decimal($valor)
    {
        return is_numeric($valor) && floor($valor) != $valor;
    }

    private function existe_ecuacion_($ecuacion_original, $familia, $genero, $epiteto) {

        $existe = Ecuacion::where('ecuacion_original', 'like', $ecuacion_original)
                            ->where(function($queryF) use($familia) {
                                $queryF->where('familia', 'like', $familia)
                                        ->orWhere(function($queryF1) use($familia) {
                                            $queryF1->whereNull('familia')
                                                    ->whereRaw("'" . $familia . "'" . " = ''");
                                                    // ->where('', '', $familia);
                                        });
                            })
                            // ->where(function($queryG) use($genero) {
                            //     $queryG->where('genero', 'like', $genero)
                            //             ->orWhere(function($queryG1) use($genero) {
                            //                 $queryG1->whereNull('genero')
                            //                         ->where('', '', $genero);
                            //             });
                            // })
                            // ->where(function($queryE) use($epiteto) {
                            //     $queryE->where('epiteto', 'like', $epiteto)
                            //             ->orWhere(function($queryE1) use($epiteto) {
                            //                 $queryE1->whereNull('epiteto')
                            //                         ->where('', '', $epiteto);
                            //             });
                            // })
                            ->first();
                            // ->toSql();
        return $existe;
    }

    private function existe_ecuacion_old($row, $ecuacion_original, $ecuacion_php, $numero_variables) {

        $ecuacion = Ecuacion::firstOrCreate([
            'variable'              => $row['variable'],
            'uso'                   => $row['uso'],
            'forma'                 => $row['forma'],
            'tipo'                  => $row['tipo'],
            'ecuacion'              => $ecuacion_php,
            'familia'               => $row['familia'],
            'genero'                => $row['genero'],
            'epiteto'               => $row['epiteto'],
            'condicion'             => $row['condicion'],
            'componente'            => $row['componente'],
            'diametro_min'          => round($row['diametro_min'], 2),
            'diametro_max'          => round($row['diametro_max'], 2),
            'numero_arboles'        => $row['numero_arboles'],
            'r2'                    => round($row['r2'], 4),
            'numero_variables'      => $numero_variables,
            'grado_putrefaccion'    => $row['grado_putrefaccion'],
            'anio_publicacion'      => $row['anio_publicacion'],
            'activo'                => $row['activo'],
            'ecuacion_original'     => $ecuacion_original,
            // 'variable_id'            => $row['variable_id'],
            'especie_id'            => $row['especie_id'],
            'id_especie_mn'         => $row['id_especie_mn'],
        ]);

        return $ecuacion;
    }
}
