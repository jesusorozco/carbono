<?php

namespace App\Imports;

use App\Models\Filtro;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class FiltroImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Filtro([
            "criterio_asignacion_id"    => $row["criterio_asignacion_id"],
            "variable_resultado"        => $row["variable_resultado"],
            "campo_1"                   => $row["campo_1"],
            "campo_2"                   => $row["campo_2"],
            "criterio"                  => $row["criterio"],
        ]);
    }
}
