<?php

namespace App\Imports;

use App\Models\Estado;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EstadosImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Estado([
            'clave'             => $row['clave'],
            'clave_renapo'      => $row['clave_renapo'],
            'nombre'            => $row['nombre'],
            'nombre_completo'   => $row['nombre_completo'],
        ]);
    }
}
