<?php

namespace App\Imports;

use App\Models\VegetacionIpcc;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class VegetacionIpccImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new VegetacionIpcc([
            'clave'     => $row['clave'],
            'nombre'    => $row['nombre'],
            'anio'      => $row['anio'],
        ]);
    }
}
