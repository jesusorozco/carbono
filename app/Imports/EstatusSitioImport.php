<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Models\Conglomerado;
use App\Models\Sitio;
use App\Models\Ecorregion;
use App\Models\VegetacionIpcc;
use App\Models\VegetacionBur;
// use App\Models\Sitio;

class EstatusSitioImport
{
    private $estatus = "";
    private $contador = 0;
    /**
    * @param Collection $collection
    */
    public function __construct()
    {
        $this->estatus = collect([]);
        $this->listar_estatus_existentes();
    }

    public function import($sitios)
    {
        foreach ($sitios as $sitio) {

            if( !$this->existe_estatus($sitio['conglomerado'], $sitio['sitio'], $sitio['ciclo']) ) {   

                $this->estatus->push(['conglomerado' => $sitio['conglomerado'], 'sitio' => $sitio['sitio'], 'ciclo' => $sitio['ciclo']]);

                try {

                    $this->contador++;

                    DB::table('estatus_sitios')->insert([
                        'ciclo'                     => $sitio['ciclo'],
                        'anio_levantamiento'        => $sitio['anio'],
                        'conglomerado'              => $sitio['conglomerado'],
                        'sitio'                     => $sitio['sitio'],
                        'estatus'                   => $sitio['estatus'],
                        'latitud'                   => $sitio['latitud'],
                        'longitud'                  => $sitio['longitud'],
                        'clave_ecorregion_n1'       => isset($sitio['clave_ecorregion_n1']) ? $sitio['clave_ecorregion_n1'] : null,
                        'clave_ecorregion_n2'       => isset($sitio['clave_ecorregion_n2']) ? $sitio['clave_ecorregion_n2'] : null,
                        'clave_ecorregion_n3'       => isset($sitio['clave_ecorregion_n3']) ? $sitio['clave_ecorregion_n3'] : null,
                        'clave_ecorregion_n4'       => isset($sitio['clave_ecorregion_n4']) ? $sitio['clave_ecorregion_n4'] : null,
                        'clave_bur'                 => $sitio['clave_bur'],
                        'clave_ipcc'                => $sitio['clave_ipcc'],
                        'vegetacion_bur_id'         => VegetacionBur::get($sitio['clave_bur'])->id,
                        'vegetacion_ipcc_id'        => VegetacionIpcc::get($sitio['clave_ipcc'])->id,
                        'ecorregion_id'             => $this->eccorregion_id($sitio),
                        'conglomerado_id'           => Conglomerado::get($sitio['conglomerado'])->id,
                        'sitio_id'                  => Sitio::get($sitio['conglomerado'], $sitio['sitio'])->id,
                    ]);

                } catch (\Throwable $th) { // Si no se pudo insertar el estatus se borra de la lista
                    $this->estatus->pop();
                    $this->contador--;
                }
            }
        }
    }

    private function existe_estatus($conglomerado, $sitio, $ciclo)
    {
        return $this->estatus
                    ->where('conglomerado', $conglomerado)
                    ->where('sitio', $sitio)
                    ->where('ciclo', $ciclo)
                    ->count() ? true : false;
    }

    private function listar_estatus_existentes()
    {
        $this->estatus = DB::table('estatus_sitios')
                ->select([
                    'conglomerado',
                    'sitio',
                    'ciclo'
                ])->get();
    }

    private function eccorregion_id(&$sitio)
    {
        $id = null;
        $contador = 4;

        while($contador > 0) {

            if( isset($sitio['clave_ecorregion_n' . $contador]) ? $sitio['clave_ecorregion_n' . $contador] : null ) {
                $id = Ecorregion::get($sitio['clave_ecorregion_n' . $contador], $contador)->id;
                $contador = 0;
            }

            $contador--;
        }

        return $id;
    }
}
