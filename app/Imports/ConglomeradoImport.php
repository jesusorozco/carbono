<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Support\Facades\DB;
use App\Models\Conglomerado;
// use App\Imports\SitioImport;
use Maatwebsite\Excel\Facades\Excel;

class ConglomeradoImport implements ToCollection, WithHeadingRow, WithChunkReading
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    private $conglomerados = "";
    private $contador = 0;
    private $sitios = "";

    public function __construct()
    {
        $this->sitios = new SitioImport;
        $this->conglomerados = collect([]);
        $this->listar_conglomerados_existentes();
    }

    function __destruct() {
        // print "Destruyendo " . __CLASS__ . "\n";
        // print $this->contador . "conglomerados insertados\n";
    }

    public function collection(Collection $collection)
    {
        foreach ($collection as $conglomerado) {

            if( !$this->existe_conglomerado($conglomerado['conglomerado']) ) {

                $this->conglomerados->push($conglomerado['conglomerado']);

                try {
                    $this->contador++;

                    Conglomerado::create([
                        'numero'            => $conglomerado['conglomerado'],
                        'upmid'             => $conglomerado['upmid'],
                    ]);
                } catch (\Throwable $th) { // Si no se pudo insertar el conglomerado se borra de la lista

                    $this->conglomerados->pop();
                    $this->contador--;
                }
            }
        }

        $this->sitios->import($collection);
    }
    
    public function chunkSize(): int
    {
        return 5000;
    }

    private function existe_conglomerado($conglomerado)
    {
        return $this->conglomerados->contains($conglomerado);
    }

    private function listar_conglomerados_existentes()
    {
        $conglomerados = Conglomerado::select([DB::raw('numero AS conglomerado')])->get();
        $this->conglomerados = $conglomerados->pluck('conglomerado');
    }
}
