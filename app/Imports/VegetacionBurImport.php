<?php

namespace App\Imports;

use App\Models\VegetacionBur;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class VegetacionBurImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new VegetacionBur([
            'clave'     => $row['clave'],
            'nombre'    => $row['nombre'],
            'anio'      => $row['anio'],
        ]);
    }
}
