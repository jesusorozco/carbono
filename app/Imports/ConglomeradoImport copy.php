<?php

namespace App\Imports;

use App\Models\Conglomerado;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Support\Facades\DB;

class ConglomeradoImport implements ToModel, WithHeadingRow, WithChunkReading
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    private $conglomerados = "";
    private $contador = 0;

    public function __construct()
    {
        $this->conglomerados = collect([]);
        $this->listar_conglomerados_existentes();
    }

    function __destruct() {
        // print "Destruyendo " . __CLASS__ . "\n";
        print $this->contador . "conglomerados insertados\n";
    }

    public function model(array $row)
    {
        print "Buscando conglomerado" . $row['conglomerado'] . "\n";
        if( $this->existe_conglomerado($row['conglomerado']) ) {

            return null;
        }

        $this->conglomerados->push($row['conglomerado']);

        try {
            $this->contador++;
            print "Insertando conglomerado\n";

            return new Conglomerado([
                'numero'            => $row['conglomerado'],
                'upmid'             => $row['upmid'],
            ]);

        } catch (\Throwable $th) { // Si no se pudo insertar el conglomerado se borra de la lista
            $this->conglomerados->pop();
            $this->contador--;
        }
    }
    
    public function chunkSize(): int
    {
        return 5000;
    }

    private function existe_conglomerado($conglomerado)
    {
        // return $this->conglomerados->firstWhere('conglomerado', $conglomerado);
        return $this->conglomerados->contains($conglomerado);
    }

    private function listar_conglomerados_existentes()
    {
        $conglomerados = Conglomerado::select([DB::raw('numero AS conglomerado')])->get();
        $this->conglomerados = $conglomerados->pluck('conglomerado');
    }
}
