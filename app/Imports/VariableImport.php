<?php

namespace App\Imports;

use App\Models\Variable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class VariableImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Variable([
            'nombre'            => $row['nombre'],
            'abreviacion'       => $row['abreviacion'],
            'unidad'            => $row['unidad'],
            'variable_id'       => $row['variable_id'],
        ]);
    }
}
