USE MuestreoNacional

DECLARE @anio_inicio INT = 2015;
DECLARE @anio_fin INT = 2030;
SELECT
	*
FROM
	(
	SELECT
		CASE WHEN ls.anio_levantamiento BETWEEN 2004 AND 2007 THEN 1 WHEN ls.anio_levantamiento BETWEEN 2009 AND 2014 THEN 2 ELSE 3 END AS ciclo,
		ls.anio_levantamiento,
		u.folio_historico AS conglomerado,
		u.folio AS upmid,
		us.folio AS sitio,
		a.numero_arbol,
		a.numero_tallo,
		a.numero_tallos_pencas AS tallos
	FROM
		Sistema.unidad_muestreo u
	LEFT JOIN
		Sistema.unidad_muestreo us ON (us.id_unidad_muestreo_padre = u.id)
	LEFT JOIN
		Sistema.levantamiento ls ON (ls.id_unidad_muestreo = us.id AND ls.anio_levantamiento <> 0)
	INNER JOIN
		Satelites.caracteristica_arbolado a ON (a.id_levantamiento = ls.id)
	WHERE
		u.id_unidad_muestreo_padre = 0
		AND u.folio_historico <> 0
		AND u.id_proyecto = 1
		AND ls.anio_levantamiento BETWEEN @anio_inicio AND @anio_fin

	UNION ALL

	SELECT
		CASE WHEN ls.anio_levantamiento BETWEEN 2004 AND 2007 THEN 1 WHEN ls.anio_levantamiento BETWEEN 2009 AND 2014 THEN 2 ELSE 3 END AS ciclo,
		ls.anio_levantamiento,
		u.folio_historico AS conglomerado,
		u.folio AS upmid,
		us.folio AS sitio,
		a.numero_arbol,
		a.numero_tallo,
		a.numero_tallos AS tallos
	FROM
		Sistema.unidad_muestreo u
	LEFT JOIN
		Sistema.unidad_muestreo us ON (us.id_unidad_muestreo_padre = u.id)
	LEFT JOIN
		Sistema.levantamiento ls ON (ls.id_unidad_muestreo = us.id AND ls.anio_levantamiento <> 0)
	INNER JOIN
		Satelites.caracteristica_vegetacion_mayor a ON (a.id_levantamiento = ls.id)
	WHERE
		u.id_unidad_muestreo_padre = 0
		AND u.folio_historico <> 0
		AND u.id_proyecto = 1
		AND ls.anio_levantamiento BETWEEN @anio_inicio AND @anio_fin
) AS t
ORDER BY
    (CASE WHEN t.anio_levantamiento BETWEEN 2004 AND 2007 THEN 1 WHEN t.anio_levantamiento BETWEEN 2009 AND 2014 THEN 2 ELSE 3 END),
    t.conglomerado,
    t.sitio
