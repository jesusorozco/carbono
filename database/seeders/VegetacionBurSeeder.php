<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Imports\VegetacionBurImport;
use Maatwebsite\Excel\Facades\Excel;

class VegetacionBurSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = "public/db/Vegetacion_BUR.xlsx";
        Excel::import(new VegetacionBurImport, $archivo);
        $this->command->getOutput()->writeln("Claves BUR insertadas!");
    }
}
