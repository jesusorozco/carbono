<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Imports\EstadosImport;
use Maatwebsite\Excel\Facades\Excel;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = "public/db/Estados.xlsx";
        Excel::import(new EstadosImport, $archivo);
        $this->command->getOutput()->writeln("Estados insertados!");
    }
}
