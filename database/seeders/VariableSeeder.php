<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Imports\VariableImport;
use Maatwebsite\Excel\Facades\Excel;

class VariableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = "public/db/Variables.xlsx";
        Excel::import(new VariableImport, $archivo);
        $this->command->getOutput()->writeln("Variables importadas!");
    }
}
