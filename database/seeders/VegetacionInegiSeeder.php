<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Imports\VegetacionInegiImport;
use Maatwebsite\Excel\Facades\Excel;

class VegetacionInegiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = "public/db/Vegetacion_INEGI.xlsx";
        Excel::import(new VegetacionInegiImport, $archivo);
        $this->command->getOutput()->writeln("Vegetación insertada!");
    }
}
