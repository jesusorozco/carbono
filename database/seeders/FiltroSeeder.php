<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Imports\FiltroImport;
use Maatwebsite\Excel\Facades\Excel;

class FiltroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = "public/db/Filtros.xlsx";
        Excel::import(new FiltroImport, $archivo);
        $this->command->getOutput()->writeln("Filtros insertados!");
    }
}
