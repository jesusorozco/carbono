<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Imports\EcorregionImport;
use Maatwebsite\Excel\Facades\Excel;

class EcorregionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = "public/db/Ecorregiones.xlsx";
        Excel::import(new EcorregionImport, $archivo);
        $this->command->getOutput()->writeln("Ecorregiones insertadas!");
    }
}
