<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Imports\EcuacionImport;
use Maatwebsite\Excel\Facades\Excel;

class EcuacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = "public/db/Ecuaciones.xlsx";
        Excel::import(new EcuacionImport, $archivo);
        $this->command->getOutput()->writeln("Ecuaciones insertadas!");
    }
}
