<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Arbol;
use Carbon\Carbon;

class ArbolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    private $arboles = [];

    public function run()
    {
        $this->muestreo_nacional(2004, 2007);
        $this->muestreo_nacional(2009, 2014);
    }

    private function muestreo_nacional($anio_inicio, $anio_fin)
    {

        $cgl_veg = DB::connection('sqlsrv')
                            ->table('MuestreoNacional.Sistema.unidad_muestreo AS u')
                            ->join('MuestreoNacional.Sistema.unidad_muestreo AS us', 'us.id_unidad_muestreo_padre', '=', 'u.id')
                            ->join('MuestreoNacional.Sistema.levantamiento AS ls', 'ls.id_unidad_muestreo', '=', 'us.id')
                            ->join('MuestreoNacional.Satelites.caracteristica_vegetacion_mayor AS a', 'a.id_levantamiento', '=', 'ls.id')
                            ->where('u.id_proyecto', 1)
                            ->where('us.id_proyecto', 1)
                            ->whereBetween('ls.anio_levantamiento', [$anio_inicio, $anio_fin])
                            ->select([
                                        'u.folio_historico AS conglomerado'
                                    ])
                            ->groupBy('u.folio_historico');
        
        $conglomerados = DB::connection('sqlsrv')
                            ->table('MuestreoNacional.Sistema.unidad_muestreo AS u')
                            ->join('MuestreoNacional.Sistema.unidad_muestreo AS us', 'us.id_unidad_muestreo_padre', '=', 'u.id')
                            ->join('MuestreoNacional.Sistema.levantamiento AS ls', 'ls.id_unidad_muestreo', '=', 'us.id')
                            ->join('MuestreoNacional.Satelites.caracteristica_arbolado AS a', 'a.id_levantamiento', '=', 'ls.id')
                            ->where('u.id_proyecto', 1)
                            ->where('us.id_proyecto', 1)
                            ->whereBetween('ls.anio_levantamiento', [$anio_inicio, $anio_fin])
                            ->select([
                                    'u.folio_historico AS conglomerado'
                                    ])
                            ->union($cgl_veg)
                            ->groupBy('u.folio_historico')
                            ->orderBy('conglomerado')
                            ->get();
                            // ->chunk(5, function($conglomerados) {
                        
                            //     foreach($conglomerados AS $conglomerado) {
                            //         echo $conglomerado->conglomerado;
                            //     }
                            // });
        // En esta versión de Laravel no funciona el chunk cuando se hace una unión
        // Así que se hará un un chunk por separado guiandome por orden de los conglomerados
        
        foreach($conglomerados->chunk(10) AS $chunk) {
            
            $this->listar_arboles($chunk->pluck('conglomerado'), $anio_inicio, $anio_fin);
        }

        $this->command->getOutput()->writeln("Arboles insertados!");
            
    }

    private function listar_arboles($conglomerado, $anio_inicio, $anio_fin) {

        $this->arboles = [];

        $vegetacion = DB::connection('sqlsrv')
                            ->table('MuestreoNacional.Sistema.unidad_muestreo AS u')
                            ->join('MuestreoNacional.Sistema.unidad_muestreo AS us', 'us.id_unidad_muestreo_padre', '=', 'u.id')
                            ->join('MuestreoNacional.Sistema.levantamiento AS ls', 'ls.id_unidad_muestreo', '=', 'us.id')
                            ->join('MuestreoNacional.Satelites.caracteristica_vegetacion_mayor AS a', 'a.id_levantamiento', '=', 'ls.id')
                            ->leftjoin('MuestreoNacional.Catalogos.especie AS e', 'e.id', '=', 'a.id_especie')
                            ->leftjoin('MuestreoNacional.Catalogos.genero AS g', 'g.id', '=', 'e.id_genero')
                            ->leftjoin('MuestreoNacional.Catalogos.familia AS f', 'f.id', '=', 'g.id_familia')
                            ->leftjoin('MuestreoNacional.Catalogos.especie_variedad AS ev', 'ev.id_especie', '=', 'e.id')
                            // ->leftjoin('Estimaciones.Anexos.ecoregion AS eco', 'eco.id_levantamiento', '=', 'ls.id')
                            // ->leftjoin('MuestreoNacional.Sistema.coordenada AS coord', 'coord.id', '=', 'ls.id_coordenada')
                            ->where('u.id_proyecto', 1)
                            ->where('us.id_proyecto', 1)
                            ->whereBetween('ls.anio_levantamiento', [$anio_inicio, $anio_fin])
                            ->whereIn('u.folio_historico', $conglomerado)
                            ->select([
                                    'ls.anio_levantamiento',
                                    'u.folio_historico AS conglomerado',
                                    'us.folio AS sitio',
                                    DB::raw("
                                        CASE
                                            WHEN a.id_condicion = 1 THEN 'Vivo'
                                            WHEN a.id_condicion = 2 THEN 'Muerto'
                                            WHEN a.id_condicion IN(3, 4) THEN 'Tocón'
                                            ELSE 'Vivo'
                                        END AS condicion
                                    "),
                                    'f.nombre AS familia',
                                    'g.nombre AS genero',
                                    'e.nombre AS epiteto',
                                    'ev.tipo AS categoria_infra',
                                    'ev.nombre AS infraespecie',
                                    'a.numero_arbol',
                                    'a.numero_tallo',
                                    'a.numero_tallos AS tallos',
                                    // 'a.diametro_altura_pecho AS diametro',
                                    DB::raw('ROUND(a.diametro_altura_pecho, 4) AS diametro'),
                                    // 'a.altura_total AS altura',
                                    DB::raw('ROUND(a.altura_total, 4) AS altura'),
                                    // 'coord.lat AS latitud',
                                    // 'coord.long AS longitud',
                                    // 'eco.clave_eco2 AS clave_ecoregion_n2',
                                    DB::raw("'arbolado' AS referencia_1"),
                                    'a.id AS referencia_2',
                                    DB::raw("'1' AS user_id")
                                    ]);
        
        $arboles = DB::connection('sqlsrv')
                    ->table('MuestreoNacional.Sistema.unidad_muestreo AS u')
                    ->join('MuestreoNacional.Sistema.unidad_muestreo AS us', 'us.id_unidad_muestreo_padre', '=', 'u.id')
                    ->join('MuestreoNacional.Sistema.levantamiento AS ls', 'ls.id_unidad_muestreo', '=', 'us.id')
                    ->join('MuestreoNacional.Satelites.caracteristica_arbolado AS a', 'a.id_levantamiento', '=', 'ls.id')
                    ->leftjoin('MuestreoNacional.Catalogos.especie AS e', 'e.id', '=', 'a.id_especie')
                    ->leftjoin('MuestreoNacional.Catalogos.genero AS g', 'g.id', '=', 'e.id_genero')
                    ->leftjoin('MuestreoNacional.Catalogos.familia AS f', 'f.id', '=', 'g.id_familia')
                    ->leftjoin('MuestreoNacional.Catalogos.especie_variedad AS ev', 'ev.id_especie', '=', 'e.id')
                    // ->leftjoin('Estimaciones.Anexos.ecoregion AS eco', 'eco.id_levantamiento', '=', 'ls.id')
                    // ->leftjoin('MuestreoNacional.Sistema.coordenada AS coord', 'coord.id', '=', 'ls.id_coordenada')
                    ->where('u.id_proyecto', 1)
                    ->where('us.id_proyecto', 1)
                    ->whereBetween('ls.anio_levantamiento', [$anio_inicio, $anio_fin])
                    ->whereIn('u.folio_historico', $conglomerado)
                    ->select([
                            'ls.anio_levantamiento',
                            'u.folio_historico AS conglomerado',
                            'us.folio AS sitio',
                            DB::raw("
                                CASE
                                    WHEN a.id_condicion = 1 THEN 'Vivo'
                                    WHEN a.id_condicion = 2 THEN 'Muerto'
                                    WHEN a.id_condicion IN(3, 4) THEN 'Tocón'
                                    ELSE 'Vivo'
                                END AS condicion
                            "),
                            'f.nombre AS familia',
                            'g.nombre AS genero',
                            'e.nombre AS epiteto',
                            'ev.tipo AS categoria_infra',
                            'ev.nombre AS infraespecie',
                            'a.numero_arbol',
                            'a.numero_tallo',
                            'a.numero_tallos_pencas AS tallos',
                            // 'a.diametro_altura_pecho AS diametro',
                            DB::raw('ROUND(a.diametro_altura_pecho, 4) AS diametro'),
                            // 'a.altura_total AS altura',
                            DB::raw('ROUND(a.altura_total, 4) AS altura'),
                            // 'coord.lat AS latitud',
                            // 'coord.long AS longitud',
                            // 'eco.clave_eco2 AS clave_ecoregion_n2',
                            DB::raw("'arbolado' AS referencia_1"),
                            'a.id AS referencia_2',
                            DB::raw("'1' AS user_id")
                            ])
                    ->unionAll($vegetacion)
                    ->orderBy('conglomerado')
                    ->get()
                    ;
        
        foreach($arboles as $arbol) {

            array_push($this->arboles, [
                'anio_levantamiento'			=> $arbol->anio_levantamiento,
                'conglomerado'					=> $arbol->conglomerado,
                'sitio'					        => $arbol->sitio,
                'condicion'					    => $arbol->condicion,
                'familia'					    => $arbol->familia,
                'genero'					    => $arbol->genero,
                'epiteto'					    => $arbol->epiteto,
                'categoria_infra'				=> $arbol->categoria_infra,
                'infraespecie'					=> $arbol->infraespecie,
                'numero_arbol'					=> $arbol->numero_arbol,
                'numero_tallo'					=> $arbol->numero_tallo,
                'tallos'					    => $arbol->tallos,
                'diametro'					    => round($arbol->diametro, 4),
                'altura'					    => round($arbol->altura, 4),
                'referencia_1'					=> $arbol->referencia_1,
                'referencia_2'					=> $arbol->referencia_2,
                'created_at'                    => Carbon::now(),
                'updated_at'                    => Carbon::now(),
            ]);
        }

        $this->insertar_arboles();
    }

    private function insertar_arboles() {
        
        Arbol::upsert(

            // Valores a insertar
            $this->arboles,

            // Valores únicos para asociar el registro
            [
                'referencia_1',
                'referencia_2',
            ],

            // Valores a actualizar en caso de encontrar el registro
            [
                'referencia_1',
                'referencia_2'
            ]
        );
    }
}
