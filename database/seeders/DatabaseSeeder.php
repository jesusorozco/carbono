<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',0
        //     'email' => 'test@example.com',
        // ]);

        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(EstadoSeeder::class);
        $this->call(EcorregionSeeder::class);
        $this->call(VegetacionBurSeeder::class);
        $this->call(VegetacionInegiSeeder::class);
        $this->call(VegetacionIpccSeeder::class);
        // $this->call(FiltroSeeder::class);
        $this->call(VariableSeeder::class);
        $this->call(FuncionSeeder::class);
        $this->call(EspecieSeeder::class);
        $this->call(EcuacionSeeder::class);
    }
}
