<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Imports\FuncionImport;
use Maatwebsite\Excel\Facades\Excel;

class FuncionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = "public/db/Funciones.xlsx";
        Excel::import(new FuncionImport, $archivo);
        $this->command->getOutput()->writeln("Funciones importadas!");
    }
}
