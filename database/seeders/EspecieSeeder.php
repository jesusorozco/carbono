<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Especie;

class EspecieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('sqlsrv')
			->table('MuestreoNacional.Catalogos.especie AS e')
			->where('e.estatus', 'Aceptado')
			->where('e.sistema', 'CONABIO')
			->where('e.familia', '!=', '-')
			->select([
				DB::raw("
					LTRIM(RTRIM(
					e.familia + ' ' +
					IIF(e.genero = '-', '', e.genero) + ' ' +
					IIF(e.nombre = '-', '', e.nombre) + ' ' +
					ISNULL(e.categoria_infra, '') + ' ' +
					ISNULL(e.infraespecie, '')
					)) AS especie
				"),
				DB::raw("IIF(e.familia= '-', NULL, e.familia) AS familia"),
				DB::raw("IIF(e.genero = '-', NULL, e.genero) AS genero"),
				DB::raw("IIF(e.nombre = '-', NULL, e.nombre) AS epiteto"),
				'e.categoria_infra',
				'e.infraespecie',
				'e.id AS id_especie_mn',
				'e.sistema',
				'e.estatus'
				])
			->orderBy('e.id')
			->chunk(500, function($especies) {

				$array = [];

				foreach ($especies as $especie) {

					array_push($array, [
						'especie'					=> $especie->especie,
						'familia'					=> $especie->familia,
						'genero'    				=> $especie->genero,
						'epiteto' 					=> $especie->epiteto,
						'categoria_infra'  			=> $especie->categoria_infra,
						'infraespecie'  			=> $especie->infraespecie,
						'id_especie_mn'  			=> $especie->id_especie_mn,
						'sistema'					=> $especie->sistema,
						'estatus'					=> $especie->estatus
					]);
				}

				Especie::upsert(
					// Valores a insertar
					$array,
					// Valores únicos para asociar el registro
					[
						'especie',
						'familia',
						'genero' ,
						'epiteto',
						'categoria_infra',
						'infraespecie',
						'id_especie_mn',
						'sistema',
						'estatus'
					],
					// Valores a actualizar en caso de encontrar el registro
					[
						'especie',
						'familia',
						'genero' ,
						'epiteto',
						'categoria_infra',
						'infraespecie',
						'id_especie_mn',
						'sistema',
						'estatus'
					]
				);

				// return false;
			});

		// foreach ($especies as $especie) {

	    //  	Especie::insert([
		// 		'especie'					=> $especie->especie,
		// 		'familia'					=> $especie->familia,
		// 		'genero'    				=> $especie->genero,
		// 		'epiteto' 					=> $especie->epiteto,
		// 		'categoria_infra'  			=> $especie->categoria_infra,
		// 		'infraespecie'  			=> $especie->infraespecie,
		// 		'id_especie_mn'  			=> $especie->id_especie_mn,
		// 		'id_especie_mn_relacion'	=> $especie->id_especie_mn_relacion,
		// 		'sistema'					=> $especie->sistema,
		// 		'estatus'					=> $especie->estatus
	    //  	]);
		// }

        $this->command->getOutput()->writeln("Especies insertadas!");
    }
}
