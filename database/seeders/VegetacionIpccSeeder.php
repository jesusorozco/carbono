<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Imports\VegetacionIpccImport;
use Maatwebsite\Excel\Facades\Excel;

class VegetacionIpccSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = "public/db/Vegetacion_IPCC.xlsx";
        Excel::import(new VegetacionIpccImport, $archivo);
        $this->command->getOutput()->writeln("Claves IPCC insertadas!");
    }
}
