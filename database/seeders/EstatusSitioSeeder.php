<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Imports\ConglomeradoImport;
use App\Models\Ecorregion;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Sitio;

class EstatusSitioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $archivo = "public/db/Estatus_sitios.xlsx";
        Excel::import(new ConglomeradoImport, $archivo);
        // $this->command->getOutput()->writeln("Conglomerados insertados!");
        // Excel::import(new EstatusSitioImport, $archivo);
    }
}
