<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('especies', function (Blueprint $table) {
            $table->id();
            $table->string('especie', 200)->nullable();
            $table->string('familia', 50)->nullable();
            $table->string('genero', 50)->nullable();
            $table->string('epiteto', 50)->nullable();
            $table->string('categoria_infra', 20)->nullable();
            $table->string('infraespecie', 50)->nullable();
            $table->integer('id_especie_mn')->nullable();
            $table->string('sistema', 30)->nullable();
            $table->string('estatus', 30)->nullable();
            // $table->timestamps();
            $table->index('familia');
            $table->index('genero');
            $table->index('epiteto');
            $table->index('id_especie_mn');
            $table->index('sistema');
            $table->index('estatus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('especies');
    }
};
