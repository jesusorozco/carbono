<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arboles', function (Blueprint $table) {
            $table->id();
            $table->integer('anio_levantamiento')->nullable();
            $table->integer('conglomerado')->nullable();
            $table->integer('sitio')->nullable();
            $table->string('condicion', 20)->nullable();
            $table->integer('especie_id')->nullable();
            $table->string('familia', 50)->nullable();
            $table->string('genero', 50)->nullable();
            $table->string('epiteto', 50)->nullable();
            $table->string('categoria_infra', 20)->nullable();
            $table->string('infraespecie', 50)->nullable();
            $table->integer('numero_arbol')->nullable();
            $table->integer('numero_tallo')->nullable();
            $table->integer('tallos')->nullable();
            $table->float('diametro', 8, 4)->nullable();
            $table->float('altura', 8, 4)->nullable();
            $table->string('referencia_1', 50)->nullable();
            $table->integer('referencia_2')->nullable();
            $table->timestamps();

            // Indices
            $table->index('anio_levantamiento');
            $table->index('conglomerado');
            $table->index('sitio');
            $table->index('numero_arbol');
            $table->index('numero_tallo');
            $table->index('referencia_1');
            $table->index('referencia_2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arbole
        s');
    }
};
