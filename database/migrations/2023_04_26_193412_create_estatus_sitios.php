<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estatus_sitios', function (Blueprint $table) {
            $table->integer('ciclo');
            $table->integer('anio_levantamiento')->nullable();
            $table->integer('conglomerado');
            $table->integer('sitio');
            $table->string('estatus', 50)->nullable();
            $table->double('latitud');
            $table->double('longitud');
            $table->string('clave_ecorregion_n1', 5)->nullable();
            $table->string('clave_ecorregion_n2', 5)->nullable();
            $table->string('clave_ecorregion_n3', 10)->nullable();
            $table->string('clave_ecorregion_n4', 10)->nullable();
            $table->string('clave_inegi', 10)->nullable();
            $table->string('clave_bur', 10)->nullable();
            $table->string('clave_ipcc', 10)->nullable();
            $table->unsignedBigInteger('conglomerado_id')->nullable();
            $table->unsignedBigInteger('sitio_id')->nullable();
            $table->unsignedBigInteger('ecorregion_id')->nullable();
            $table->unsignedBigInteger('vegetacion_inegi_id')->nullable();
            $table->unsignedBigInteger('vegetacion_bur_id')->nullable();
            $table->unsignedBigInteger('vegetacion_ipcc_id')->nullable();
            $table->timestamps();

            // Relaciones
            $table->foreign('conglomerado_id')->references('id')->on('conglomerados')->onDelete('cascade');
            $table->foreign('sitio_id')->references('id')->on('sitios')->onDelete('cascade');
            $table->foreign('ecorregion_id')->references('id')->on('ecorregiones')->onDelete('cascade');
            $table->foreign('vegetacion_inegi_id')->references('id')->on('vegetacion_inegi')->onDelete('cascade');
            $table->foreign('vegetacion_bur_id')->references('id')->on('vegetacion_bur')->onDelete('cascade');
            $table->foreign('vegetacion_ipcc_id')->references('id')->on('vegetacion_ipcc')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estatus_sitios');
    }
};
