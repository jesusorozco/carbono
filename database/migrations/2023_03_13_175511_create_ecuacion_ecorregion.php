<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecuacion_ecorregion', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ecuacion_id');
            $table->unsignedBigInteger('ecorregion_id');
            $table->timestamps();

            // Llaves foraneas y restricción contra duplicados
            $table->foreign('ecuacion_id')->references('id')->on('ecuaciones')->onDelete('cascade');
            $table->foreign('ecorregion_id')->references('id')->on('ecorregiones')->onDelete('cascade');
            $table->unique(['ecuacion_id', 'ecorregion_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecuacion_ecorregion');
    }
};
