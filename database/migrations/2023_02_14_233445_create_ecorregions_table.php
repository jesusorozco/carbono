<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecorregiones', function (Blueprint $table) {
            $table->id();
            $table->string('clave', 10);
            $table->string('descripcion', 150);
            $table->integer('nivel');
            $table->string('clave_eco1', 5)->nullable();
            $table->string('clave_eco2', 5)->nullable();
            $table->string('clave_eco3', 10)->nullable();
            $table->string('clave_eco4', 10)->nullable();
            $table->unsignedBigInteger('ecorregion_id')->nullable();
            $table->foreign('ecorregion_id')->references('id')->on('ecorregiones')->onDelete('cascade');

            // Indices
            $table->index('clave');
            $table->index('clave_eco1');
            $table->index('clave_eco2');
            $table->index('clave_eco3');
            $table->index('clave_eco4');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecorregiones');
    }
};
