<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sitios', function (Blueprint $table) {
            $table->id();
            $table->integer('conglomerado')->nullable();
            $table->integer('numero')->nullable();
            $table->string('identificador', 20)->nullable();
            $table->float('superficie_metros')->nullable();
            $table->unsignedBigInteger('conglomerado_id')->nullable();
            $table->foreign('conglomerado_id')->references('id')->on('conglomerados')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sitios');
    }
};
