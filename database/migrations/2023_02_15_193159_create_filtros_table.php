<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filtros', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('arbol_asignacion_id');
            $table->string('variable_resultado', 10);
            $table->string('campo_1', 50);
            $table->string('campo_2', 50)->nullable();
            $table->string('criterio', 50);
            $table->integer('user_id')->nullable();
            $table->timestamps();

            // Índices
            $table->index('campo_1');
            $table->index('campo_2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filtros');
    }
};
