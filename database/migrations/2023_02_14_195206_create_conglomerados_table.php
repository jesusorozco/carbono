<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conglomerados', function (Blueprint $table) {
            $table->id();
            $table->integer('numero')->nullable();
            $table->integer('upmid')->nullable();
            $table->string('identificador', 20)->nullable();
            // $table->string('alias', 20)->nullable();
            $table->float('superficie_metros')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conglomerados');
    }
};
