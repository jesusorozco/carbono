<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecuaciones', function (Blueprint $table) {
            $table->id();
            $table->string("variable", 10);
            $table->string("uso", 50)->nullable();
            $table->string("forma", 50)->nullable();
            $table->string("tipo", 100)->nullable();
            $table->string("ecuacion", 700);
            $table->string("familia", 50)->nullable();
            $table->string("genero", 50)->nullable();
            $table->string("epiteto", 50)->nullable();
            $table->string("condicion", 50)->nullable();
            $table->string("componente", 50)->nullable();
            $table->float("diametro_min", 8, 2)->nullable();
            $table->float("diametro_max", 8, 2)->nullable();
            $table->integer("numero_arboles")->nullable();
            $table->float("r2", 8, 4)->nullable();
            $table->integer("numero_variables")->default(0);
            $table->integer("grado_putrefaccion")->nullable();
            $table->integer("anio_publicacion")->nullable();
            $table->boolean("activo")->default(1);
            $table->string("ecuacion_original", 700);
            $table->integer("variable_id")->nullable();
            $table->integer("especie_id")->nullable();
            $table->integer("id_especie_mn")->nullable();
            $table->timestamps();

            // $table->string("clave_ecorregion_n1", 20)->nullable();
            // $table->string("clave_ecorregion_n2", 20)->nullable();
            // $table->string("clave_ecorregion_n3", 20)->nullable();
            // $table->string("clave_ecorregion_n4", 20)->nullable();
            // $table->string("clave_bur", 20)->nullable();
            // $table->string("clave_estado_inegi", 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecuaciones');
    }
};
