<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vegetacion_inegi', function (Blueprint $table) {
            $table->id();
            $table->string('clave', 10);
            $table->string('nombre', 100);
            $table->string('fase', 15);
            $table->string('clave_base', 5)->nullable();
            $table->string('clave_fase', 5)->nullable();
            $table->string('tipo', 50);
            $table->boolean('es_primaria');
            $table->boolean('es_secundaria');
            $table->boolean('es_bosque');
            $table->string('serie', 20)->nullable();
            $table->integer('id_mn')->nullable();
            $table->boolean('activo')->default(1);

            // Indices
            $table->index('fase');
            $table->index('tipo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vegetacion_inegi');
    }
};
